const express = require('express');

const app = express();

const configComponents = require('./config/components');
configComponents(app);

const configMiddleware = require('./middleware/middleware');
configMiddleware(app);

module.exports = app;
