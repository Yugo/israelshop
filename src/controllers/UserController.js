const fs = require('fs');
const bcrypt = require('bcrypt');
const mongo = require('mongodb');
const jwt = require('jsonwebtoken');

const ApiError = require('../config/errors/ApiError');
const DBError = require('../db/errors/DBError');

const config = require('../config/config');
const constants = require('../config/constants');

class UserController {
	static get project() {
		return { name: 1, surname: 1, login: 1, location: 1 };
	}

	constructor(db) {
		this.db = db;
		this.coll = this.db.collection('user');
		this.collCart = this.db.collection('cart');
	}

	async createUser(req, res, next) {
		const { email, passport_number, password, confirm_password } = req.body;
		if (!passport_number || !email || !password || !confirm_password) {
			return next(new ApiError('Missing required field', 400));
		}

		const isUserExists = await this.isUserExists({ passport_number }, { email });
		if (isUserExists) {
			return next(new ApiError('User exists', 400));
		} else if (isUserExists === null) {
			return next(new DBError('Something went wrong', 500));
		}

		if (password !== confirm_password) {
			return next(new ApiError('Wrong confirmation password', 400));
		}

		try {
			const salt = bcrypt.genSaltSync(config.crypto.saltRounds);
			const hash = bcrypt.hashSync(password, salt);
			const _created_at = Date.now();
			const role = constants.ROLES.CLIENT;
			const status = constants.STATUSES.IDLE;

			const r = await this.coll.insertOne({
				email,
				passport_number,
				password: hash,
				_created_at,
				role,
				status});
			const userId = r.insertedId.toString();

			// CREATE CART FOR USER
			await this.collCart.insertOne({ _id: userId, items: [] });

			return res.status(200).json({ message: 'User successfully registered', userId });
		} catch(error) {
			return next(error);
		}
	}

	async finishRegistration(req, res, next) {
		const { login, name, surname, city, address } = req.body;
		const id = req.params.id;
		if ([login, name, surname, city, address].some(field => !field)) {
			return next(new ApiError('Missing required field', 400));
		}
		const foundUser = await this.findUserById(id);
		if (!foundUser) {
			return next(new ApiError('User doesn\'t exist', 404));
		}
		try {
			const _updated_at = Date.now();
			const location = { city, address };

			const r = await this.coll.findOneAndUpdate(
				{ _id: new mongo.ObjectID(id) },
				{ $set: {
					login, name, surname, location, _updated_at
				} },
				{ returnOriginal: false });

			// TODO: as a strong validation send a letter to verify registration
			return res.status(200).json({ message: 'User data successfully updated' });
		} catch(error) {
			return next(error);
		}
	}

	async auth(req, res, next) {
		const { login, password } = req.body;
		if (!login || !password) {
			return next(new ApiError('Missing required field', 400));
		}
		try {
			let foundUser = await this.findUserByLogin(login);
			if (!foundUser) {
				return next(new ApiError('User doesn\'t exist', 404));
			}

			const isValidPassword = bcrypt.compareSync(password, foundUser.password);
			if (!isValidPassword) {
				return next(new ApiError('Invalid password', 403));
			}

			const expires_in = 604800000;
      const token = this.generateToken(foundUser, String(expires_in));

			return res.status(200).json({ message: 'Successfully authorized', token, expires_in });
		} catch(error) {
			return next(error);
		}
	}

	async getMe(req, res, next) {
    const RSA_PUBLIC_KEY = fs.readFileSync('./public.key.pub');
    const decoded = jwt.verify(req.get('Authorization'), RSA_PUBLIC_KEY);
    const { id } = decoded;

	  try {
	    const user = await this.findUserById(id);
	    res.status(200).json({ user });
    } catch(error) {
	    return next(error);
    }
  }

	async findUserById(id) {
		try {
			let user = await this.coll.find({ _id: new mongo.ObjectID(id) }, UserController.project).toArray();
			user = user.length === 1 ? user[0] : null;
			return user;
		} catch(error) {
			return null;
		}
	}

	async findUserByLogin(login) {
		try {
			let foundUser = await this.coll.find(
				{ $or: [{ login }, { email: login }] },
				UserController.project
			).toArray();
			foundUser = foundUser.length === 1 ? foundUser[0] : null;
			return foundUser;
		} catch(error) {
			return null;
		}
	}

	async isUserExists(...fields) {
		try {
			const user = await this.coll.find({ $or: fields }).toArray();
			return !!user.length;
		} catch(error) {
			return null;
		}
	}

	async updateUserInfo() {
		// TBD
	}

	async deleteUser() {
		// TBD
	}

	generateToken(user, expiresIn) {
    const RSA_PRIVATE_KEY = fs.readFileSync('./private.key');

    return jwt.sign(
      {
        id: user._id.toString(),
        login: user.login || user.email,
        role: user.role
      }, RSA_PRIVATE_KEY,
      { algorithm: 'RS256', expiresIn });
  }
}

module.exports = UserController;
