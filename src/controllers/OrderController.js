const ApiError = require('../config/errors/ApiError');
const DBError = require('../db/errors/DBError');
const fs = require('fs');
const jwt = require('jsonwebtoken');

const constants = require('../config/constants');

class OrderController {
	static getDateInThreeDays() {
		const today = new Date();
		const inThreeDays = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 4);
		return inThreeDays.getTime();
	}

	static get project() {
	  return { items: 1, _id: 0 };
  }

	constructor(db) {
		this.db = db;
		this.coll = this.db.collection('order');
	}

	async placeOrder(req, res, next) {
    const RSA_PUBLIC_KEY = fs.readFileSync('./public.key.pub');
    const decoded = jwt.verify(req.get('Authorization'), RSA_PUBLIC_KEY);
    const { id } = decoded;

		let { items, city, address } = req.body;

		if (!items || !city || !address) {
			return next(new ApiError('Missing required field', 400));
		}
		if (items.length < 1) {
			return next(new ApiError('Order is empty', 400));
		}
		try {
			const user_id = id;
			const status = constants.ORDER_STATUSES.UNPAID;
			const delivery_info = {
				date: OrderController.getDateInThreeDays(),
				city,
				address
			};

			await this.coll.insertOne({
				user_id,
				delivery_info,
				items,
				status
			});
			return res.status(200).json({ message: 'Order successfully placed' });
		} catch(error) {
			return next(error);
		}
	}

	async getUserOrders(req, res, next) {
    const RSA_PUBLIC_KEY = fs.readFileSync('./public.key.pub');
    const decoded = jwt.verify(req.get('Authorization'), RSA_PUBLIC_KEY);
    const { id } = decoded;

	  try {
	    const orders = await this.findOrders(id);
	    if (orders === null) {
	      return next(new DBError('Something went wrong'));
      }
	    res.status(200).json({ orders: orders });
    } catch(error) {
	    return next(error);
    }
  }

  async getOrders(req, res, next) {
	  try {
	    const orders = await this.coll.find().toArray();
	    res.status(200).json({ orders });
    } catch(error) {
	    return next(error);
    }
  }

	async findOrders(user_id) {
	  user_id = user_id ? user_id : { $exists: true };

    try {
      const orders = await this.coll.find({ user_id }).toArray();
      console.log(orders);
      return orders;
    } catch(error) {
      return null;
    }
	}
}

module.exports = OrderController;
