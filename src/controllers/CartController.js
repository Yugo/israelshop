const mongo = require('mongodb');
const fs = require('fs');
const jwt = require('jsonwebtoken');

const ApiError = require('../config/errors/ApiError');
const DBError = require('../db/errors/DBError');

class CartController {
	constructor(db) {
		this.db = db;
		this.coll = this.db.collection('cart');
	}

	async findCartByUserId(id) {
		try {
			const cart = await this.coll.find({ _id: id })
				.project({ _id: 0, items: 1 })
				.toArray();
			return cart[0].items;
		} catch(error) {
			return null;
		}
	}

	async isProductInCart(id, product_id) {
		try {
			const items = await this.coll.find({ $and: [{ _id: id }, { 'items.product_id': product_id }] })
				.project({ _id: 0, items: 1 })
				.toArray();
			return !!items.length;
		} catch(error) {
			return null;
		}
	}

	async clearCart(req, res, next) {
    const RSA_PUBLIC_KEY = fs.readFileSync('./public.key.pub');
    const decoded = jwt.verify(req.get('Authorization'), RSA_PUBLIC_KEY);
    const { id } = decoded;

		try {
			const r = this.coll.findOneAndUpdate(
				{ _id: id },
				{ $pull: { items: { $exists: true } } },
				{ returnOriginal: false });
			return res.status(200).json({ cart: r.value });
		} catch(error) {
			return next(error);
		}
	}

	async getCart(req, res, next) {
	  const RSA_PUBLIC_KEY = fs.readFileSync('./public.key.pub');
	  const decoded = jwt.verify(req.get('Authorization'), RSA_PUBLIC_KEY);

		try {
			const items = await this.findCartByUserId(decoded.id);
			if (items === null) {
				return next(new DBError('Something went wrong'));
			}
			return res.status(200).json({ items });
		} catch(error) {
			return next(error);
		}
	}

	async getItemFromCart(req, res, next) {
    const RSA_PUBLIC_KEY = fs.readFileSync('./public.key.pub');
    const decoded = jwt.verify(req.get('Authorization'), RSA_PUBLIC_KEY);
    const { id } = decoded;
    const product_id = req.params.productId;

    try {
      let item = await this.coll.aggregate([
        { $unwind: '$items' },
        { $match: { 'items.product_id': product_id } },
        { $project: { _id: 0, items: 1 } }
      ]).toArray();
      console.log(item);
      item = item.length ? item[0].items : {};

      res.status(200).json(item);
    } catch(error) {
      return next(error);
    }
  }

	async addOrUpdateItemInCart(req, res, next) {
    const RSA_PUBLIC_KEY = fs.readFileSync('./public.key.pub');
    const decoded = jwt.verify(req.get('Authorization'), RSA_PUBLIC_KEY);
    const { id } = decoded;

		const { product_id, title, price, count, img_url } = req.body;
		if (!product_id || !title || !price || typeof count !== 'number') {
			return next(new ApiError('Missing required field', 400));
		}
		try {
			const isProductInCart = await this.isProductInCart(id, product_id);
			let r;
			if (isProductInCart) {
			  if (count > 0) {
          r = await this.coll.findOneAndUpdate(
            { _id: id, 'items.product_id': product_id },
            { $set: { 'items.$.count': +count } },
            { returnOriginal: false })
        } else {
          r = await this.coll.findOneAndUpdate(
            { _id: id },
            { $pull: { items: { product_id } } },
            { returnOriginal: false }
          );
        }
			} else {
				r = await this.coll.findOneAndUpdate(
					{ _id: id },
					{ $push: { items: { product_id, title, price: +price, count: +count, img_url } } },
					{ returnOriginal: false }
				)
			}
			return res.status(200).json({ items: r.value.items });
		} catch(error) {
			return next(error);
		}
	}

	async deleteItemFromCart(req, res, next) {
    const RSA_PUBLIC_KEY = fs.readFileSync('./public.key.pub');
    const decoded = jwt.verify(req.get('Authorization'), RSA_PUBLIC_KEY);
    const { id } = decoded;

		const product_id = req.params.productId;

		try {
			const r = await this.coll.findOneAndUpdate(
				{ _id: id },
				{ $pull: { items: { product_id } } },
				{ returnOriginal: false }
			);
			return res.status(200).json({ items: r.value.items });
		} catch(error) {
			return next(error);
		}
	}
}

module.exports = CartController;
