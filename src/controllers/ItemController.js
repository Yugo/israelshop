const mongo = require('mongodb');

const DBError = require('../db/errors/DBError');
const ApiError = require('../config/errors/ApiError');

class ItemController {
	static get FIELDS() {
		return ['title', 'description', 'category', 'price', 'img_url'];
	}

	static normalizeFields(body) {
		const normalizedFields = {};
		Object.keys(body).forEach(field => {
			if (ItemController.FIELDS.find(check => check === field)) {
				if (field === 'price') {
					body[field] = +body[field];
				}
				normalizedFields[field] = body[field];
			}
		});
		return normalizedFields;
	}

	constructor(db) {
		this.db = db;
		this.coll = this.db.collection('item');
	}

	async _findItems() {
		try {
			const items = await this.coll.find().toArray();
			return items;
		} catch(error) {
			return null;
		}
	}

	async _findItemById(id) {
		try {
			const item = await this.coll.find({ _id: new mongo.ObjectID(id) }).toArray();
			return item;
		} catch(error) {
			return null;
		}
	}

	async _findItemsByCategory(category) {
		try {
			const items = await this.coll.find({ category }).toArray();
			return items;
		} catch(error) {
			return null;
		}
	}

	async _searchItems(query) {
		try {
			const items = await this.coll.find({ $text: { $search: query } }).toArray();
			return items;
		} catch(error) {
			return null;
		}
	}

	async getItems(req, res, next) {
		const query = req.query.search;
		const category = req.query.category;

		let items;

		if (query) {
			items = await this._searchItems(query);
		} else if (category && category !== 'All') {
			items = await this._findItemsByCategory(category);
		} else {
			items = await this._findItems();
		}

		if (!items) {
			return next(new DBError('Something went wrong'));
		}
		return res.status(200).json({ items });
	}

	async getItemById(req, res, next) {
		const id = req.params.id;
		const item = await this._findItemById(id);

		if (item) {
			if (item.length) {
				return res.status(200).json(item[0]);
			} else {
				return next(new ApiError('Item not found', 404));
			}
		} else {
			return next(new DBError('Something went wrong'));
		}
	}

	async addItem(req, res, next) {
		const { title, description, price, img_url, category } = req.body;
		if (!title || !price || !category) {
			return next(new ApiError('Missing required field', 400));
		}
		try {
			await this.coll.insertOne({
				title,
				price: +price,
				category,
				description: description || '',
				img_url: img_url || ''
			});
			return res.status(200).json({ message: 'Successfully created' });
		} catch(error) {
			return next(error);
		}
	}

	async patchItem(req, res, next) {
		const id = req.params.id;
		const fields = ItemController.normalizeFields(req.body);
		if (!id) {
			return next(new ApiError('Missing ID param', 400));
		}

		try {
			const r = await this.coll.findOneAndUpdate(
				{ _id: new mongo.ObjectID(id) },
				{ $set: fields },
				{
					upsert: true,
					returnOriginal: false
				});
			return res.status(200).json({ item: r.value });
		} catch(error) {
			return next(error);
		}
	}

	async deleteItem(req, res, next) {
		const id = req.params.id;
		if (!id) {
			return next(new ApiError('Missing ID param', 400));
		}
		try {
			const r = await this.coll.deleteOne({ _id: new mongo.ObjectID(id) });
			return res.status(200).json({ message: 'Successfully deleted' });
		} catch(error) {
			return next(error);
		}
	}

  async getCategories(req, res, next) {
	  try {
	    const categories = await this.coll.aggregate([
        { $project: { category: 1, _id: 0 } },
        { $group: { _id: '$category', total: { $sum: 1 } } }
      ]).toArray();
	    return res.status(200).json({ categories });
    } catch(error) {
	    return next(error);
    }
  }
}

module.exports = ItemController;
