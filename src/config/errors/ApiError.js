class ApiError extends Error {
    constructor(message, status = 500) {
        super(message);
        this.name = this.constructor.name;
        this.status = status;
        this.message = message;
    }
}

module.exports = ApiError;
