module.exports = {
    port: 3000,

    sessionMaxAge: process.env.SESSION_MAXAGE,
    db: {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dbName: process.env.DB_NAME,
        user: process.env.DB_LOGIN,
        pwd: process.env.DB_PASS
    }
};
