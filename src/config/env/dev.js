module.exports = {
    sessionMaxAge: 86400000, // one day
    port: 3000,
    db: {
        host: 'mongodb://localhost',
        port: 27017,
        dbName: 'mall',
    },
    crypto: {
        saltRounds: 10
    }
};
