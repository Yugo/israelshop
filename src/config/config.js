function loadConfig(env = 'dev') {
    try {
        const configPath = `./env/${env}`;
        // check if config exists
        require.resolve(configPath);

        return require(configPath);
    } catch (err) {
        console.error(`Config for NODE_ENV=${env} not found`);
        process.exit(1);
    }
}

const env = process.env.NODE_ENV || 'dev';

module.exports = loadConfig(env);
