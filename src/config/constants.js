module.exports = {
    ROLES: {
        ADMIN: 'admin',
        MANAGER: 'manager',
        CLIENT: 'client',
        GUEST: 'guest'
    },
    STATUSES: {
        IDLE: 'idle',
        SHOPPING: 'shopping',
        ORDERING: 'ordering'
    },
    ORDER_STATUSES: {
        UNPAID: 'unpaid',
        PAID_IN_WORK: 'paid_in_work',
        PAID_CLOSED: 'paid_closed'
    }
};
