const DBContext = require('../db/DBContext');

const UserController = require('../controllers/UserController');
const ItemController = require('../controllers/ItemController');
const OrderController = require('../controllers/OrderController');
const CartController = require('../controllers/CartController');

const config = require('./config');

module.exports = async function(app) {
    const components = {};

    components.DB = await new DBContext().connect({
        url: `${config.db.host}:${config.db.port}`,
        dbName: config.db.dbName,
        connectionOptions: {
            useNewUrlParser: true
        }
    });

    components.UserController = new UserController(components.DB);
    components.ItemController = new ItemController(components.DB);
    components.OrderController = new OrderController(components.DB);
    components.CartController = new CartController(components.DB);

    app.locals.components = components;
};
