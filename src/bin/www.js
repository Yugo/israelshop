#!/usr/bin/env node

const app = require('../app');
const config = require('../config/config');

app.listen(config.port, '0.0.0.0', err => {
    if (err) return console.error(err);
    console.log(`Running on port ${config.port}...`);
});
