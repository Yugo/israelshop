const ApiError = require('../../config/errors/ApiError');
const DBError = require('../../db/errors/DBError');

function handleApiError(response, error) {
    response.status(error.status).json({
        error: {
            code: error.code,
            message: error.message,
        },
    });
}

function handleDBError(response, error) {
    response.status(error.status || 500).json({
        error: {
            message: error.message,
            name: error.name
        }
    });
}

function handleSyntaxError(response, error) {
    let result = {
        error: {
            code: 30,
            message: error.message,
            details: error.errors,
        },
    };
    if (error.code) {
        result.error.code = error.code;
    }
    response.status(400).json(result);
}

module.exports = (error, request, response, next) => {
    console.error(error);

    if (error instanceof ApiError) {
        return handleApiError(response, error);
    }
    if (error instanceof DBError) {
        return handleDBError(response, error);
    }
    if (error instanceof SyntaxError) {
        return handleSyntaxError(response, error);
    }

    response.status(error.status || 500).json({
        error: {
            code: 21,
            message: 'Server internal error, please try again',
            details: error.errors,
        },
    });
};
