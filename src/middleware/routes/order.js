const router = require('express').Router();
const validateClient = require('../validateClient');

router.post('/', validateClient, (req, res, next) => {
	req.app.locals.components.OrderController.placeOrder(req, res, next);
});

module.exports = router;
