const router = require('express').Router();

router.get('/', (req, res, next) => {
  req.app.locals.components.UserController.getMe(req, res, next);
});

module.exports = router;
