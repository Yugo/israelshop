const router = require('express').Router();
const validateClient = require('../../middleware/validateClient');

router.get('/', validateClient, (req, res, next) => {
	req.app.locals.components.CartController.getCart(req, res, next);
});

router.get('/:productId', validateClient, (req, res, next) => {
  req.app.locals.components.CartController.getItemFromCart(req, res, next);
});

router.patch('/', validateClient, (req, res, next) => {
	req.app.locals.components.CartController.addOrUpdateItemInCart(req, res, next);
});

router.delete('/clear', validateClient, (req, res, next) => {
	req.app.locals.components.CartController.clearCart(req, res, next);
});

router.delete('/:productId', validateClient, (req, res, next) => {
	req.app.locals.components.CartController.deleteItemFromCart(req, res, next);
});

module.exports = router;
