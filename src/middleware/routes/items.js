const router = require('express').Router();

router.get('/', (req, res, next) => {
	req.app.locals.components.ItemController.getItems(req, res, next);
});

module.exports = router;
