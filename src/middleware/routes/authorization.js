const router = require('express').Router();

router.post('/', (req, res, next) => {
	req.app.locals.components.UserController.auth(req, res, next);
});

module.exports = router;
