const router = require('express').Router();
const validateManager = require('../validateManager');

router.get('/:id', (req, res, next) => {
	req.app.locals.components.ItemController.getItemById(req, res, next);
});

router.post('/', validateManager, (req, res, next) => {
	req.app.locals.components.ItemController.addItem(req, res, next);
});

router.patch('/:id', validateManager, (req, res, next) => {
	req.app.locals.components.ItemController.patchItem(req, res, next);
});

router.delete('/:id', validateManager, (req, res, next) => {
	req.app.locals.components.ItemController.deleteItem(req, res, next);
});

module.exports = router;
