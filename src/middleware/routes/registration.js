const router = require('express').Router();
const validate = require('../validateClient');

router.post('/', (req, res, next) => {
	req.app.locals.components.UserController.createUser(req, res, next);
});

router.post('/:id', validate, (req, res, next) => {
	req.app.locals.components.UserController.finishRegistration(req, res, next);
});

module.exports = router;
