const router = require('express').Router();
const validateClient = require('../validateClient');
const validateManager = require('../validateManager');

router.get('/', validateClient, (req, res, next) => {
  req.app.locals.components.OrderController.getUserOrders(req, res, next);
});

router.get('/all', validateManager, (req, res, next) => {
  req.app.locals.components.OrderController.getOrders(req, res, next);
});

module.exports = router;
