const router = require('express').Router();

router.get('/', (req, res, next) => {
  req.app.locals.components.ItemController.getCategories(req, res, next);
});

module.exports = router;
