const ApiError = require('../config/errors/ApiError');
const jwt = require('jsonwebtoken');
const fs = require('fs');

const constants = require('../config/constants');
const RSA_PUBLIC_KEY = fs.readFileSync('./public.key.pub');

module.exports = (req, res, next) => {
  const token = req.get('Authorization');
  if (!token) {
    return next(new ApiError('Unauthorized, please verify', 401));
  }
  const decoded = jwt.verify(token, RSA_PUBLIC_KEY);
  if (decoded.role !== constants.ROLES.MANAGER) {
    return next(new ApiError('Permission Denied', 403));
  }
  return next();
};
