const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const logger = require('morgan');

const config = require('../config/config');

// ROUTES
const routeRegister = require('./routes/registration');
const routeAuth = require('./routes/authorization');
const routeMe = require('./routes/me');
const routeItem = require('./routes/item');
const routeItems = require('./routes/items');
const routeOrder = require('./routes/order');
const routeOrders = require('./routes/orders');
const routeCart = require('./routes/cart');
const routeCategories = require('./routes/categories');

// ERROR HANDLERS
const errorHandlerGeneral = require('./errors/generalHandler');

module.exports = (app) => {
    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({ limit: '50mb', extended: false, parameterLimit:50000}));
    app.use(cookieParser());
    app.use(cors());

    app.use(logger('dev'));
    app.use((req, res, next) => {
        let logStr = `${req.method} ${req.url} HTTP/${req.httpVersion}\n`;
        logStr += '-------------------------------------------------------\n';
        logStr += 'Headers:\n';
        logStr += '-------------------------------------------------------\n';
        Object.keys(req.headers).forEach((key) => {
            logStr += `${key}: ${req.headers[key]}\n`;
        });
        logStr += '-------------------------------------------------------\n';
        logStr += 'Body:\n';
        logStr += '-------------------------------------------------------\n';
        logStr += `${JSON.stringify(req.body, null, 2)}\n`;
        logStr += '-------------------------------------------------------\n';
        console.log(logStr);
        next();
    });

	// Check if API works fine, always return 200 OK
	app.use('/health', (req, res, next) => res.sendStatus(200));

    app.use('/register', routeRegister);
    app.use('/auth', routeAuth);
    app.use('/me', routeMe);
    app.use('/item', routeItem);
    app.use('/items', routeItems);
    app.use('/order', routeOrder);
    app.use('/orders', routeOrders);
    app.use('/cart', routeCart);
    app.use('/categories', routeCategories);

    // DEFAULT ROUTING
    app.use(express.static(__dirname + '/../application'));
    app.all('/*', (req, res, next) => {
        res.sendFile('index.html', { root: __dirname + '/../application' });
    });
};
