const MongoClient = require('mongodb').MongoClient;

class DBContext {
    constructor() {}

    async connect(options) {
        const client = await MongoClient.connect(options.url, options.connectionOptions);
		return client.db(options.dbName);
    }
}

module.exports = DBContext;
